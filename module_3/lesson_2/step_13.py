from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time


def send_form(link):
    browser = webdriver.Chrome()
    browser.get(link)

    browser.find_element(By.CSS_SELECTOR, "input.first[required]").send_keys("First")
    browser.find_element(By.CSS_SELECTOR, "input.second[required]").send_keys("Second")
    browser.find_element(By.CSS_SELECTOR, "input.third[required]").send_keys("Third")
    
    # Отправляем заполненную форму
    button = browser.find_element(By.CSS_SELECTOR, "button.btn")
    button.click()
    time.sleep(1)

    # находим элемент, содержащий текст
    welcome_text_elt = browser.find_element(By.TAG_NAME, "h1")
    
    return welcome_text_elt.text


class TestRegistrationForm(unittest.TestCase):
    link = "http://suninjuly.github.io/registration1.html"
    link1 = "http://suninjuly.github.io/registration2.html"

    def test_sendform_Link1(self):
        unittest.assertEqual("Congratulations! You have successfully registered!", send_form(self.link))

    def test_sendform_Link1(self):
        unittest.assertEqual("Congratulations! You have successfully registered!", send_form(self.link1))


if __name__ == "main":
    unittest.main()
