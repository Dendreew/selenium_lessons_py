from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math

link_text = str(math.ceil(math.pow(math.pi, math.e)*10000))
timeout = 50
link = "http://suninjuly.github.io/find_link_text"
value1 = "input"
value2 = "last_name"
value3 = "form-control city"

try:
    browser = webdriver.Chrome()
    browser.get(link)

    link = browser.find_element(By.LINK_TEXT, link_text)
    link.click()

    input1 = browser.find_element(By.TAG_NAME, value1)
    input1.send_keys("Ivan")
    input2 = browser.find_element(By.NAME, value2)
    input2.send_keys("Petrov")
    input3 = browser.find_element(By.CLASS_NAME, "city")
    input3.send_keys("Smolensk")
    input4 = browser.find_element(By.ID, "country")
    input4.send_keys("Russia")
    button = browser.find_element(By.CSS_SELECTOR, "button.btn")
    button.click()

finally:
    # успеваем скопировать код за 30 секунд
    time.sleep(timeout)
    # закрываем браузер после всех манипуляций
    browser.quit()